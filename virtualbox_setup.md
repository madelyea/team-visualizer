## Install and setup VirtualBox, Raspbian-buster desktop

You should be able to get by with 1GB ram, 8GB disk (We now have 7GB of video and audio files, this may no longer be true). 

### Mac or Windows

https://thepi.io/how-to-run-raspberry-pi-desktop-on-windows-or-macos/

### Linux

`sudo apt install virtualbox`

**Note**: if you are not seeing 64 bit options in the `Version` dropdown when creating a new virtual machine go [here](http://www.fixedbyvonnie.com/2014/11/virtualbox-showing-32-bit-guest-versions-64-bit-host-os/).

[Raspbian-buster Desktop iso](https://www.raspberrypi.org/downloads/raspberry-pi-desktop/)

### Network connectivity for host OS

https://www.tecmint.com/network-between-guest-vm-and-host-virtualbox/

### Purge and remove unnecessary programs

    sudo apt-get remove --purge libreoffice* openjdk-11-jre-headless openjdk-11-jre openjdk-11-jdk-headless openjdk-11-jdk libopenjfx-jni scratch2 nuscratch sonic-pi

Then reboot

    sudo apt-get clean

    sudo apt-get autoremove --purge

    sudo apt-get update && sudo apt-get upgrade -y

### Helpful tools

    sudo apt-get install tmux fish vim

### Install Guest Additions

This will allow you to do things like resize the VirtualBox window and have it automatically resize everything to this new aspect ratio/resolution, copy/paste between your main computer and VirtualBox, share folders between your main computer and VirtualBox, etc...

First off, when initially creating your VirtualBox instance, you need to make sure it's set up as Debian (32-bit), not Debian (64-bit).

Before you start up this VirtualBox instance, enter the "Settings" menu for it as seen below, click "Display", and change the "Graphics Controller" option to VBoxSVGA. This will allow the screen resizing feature of Guest Additions to work.

![VBoxSVGA](https://i.imgur.com/VObQW7m.png)

Once you've got a 32-bit Debian Buster instance up and running in VirtualBox, enter this into your terminal:
    
    sudo apt-get install build-essential module-assistant
    
Click "Devices" up top, and click "Insert Guest Additions CD image..."

![Insert Guest Additions CD image...](https://i.imgur.com/3wE6Q3I.png)

Then enter this into your terminal:

    sudo sh /media/cdrom/VBoxLinuxAdditions.run
    
Reboot your VirtualBox instance, and some of the Guest Additions features should work. As far as I can tell, "Shared Folders", "Shared Clipboard", and auto screen resizing work just fine. "Drag and Drop" doesn't work for whatever reason, but fortunately "Shared Folders" eliminates the need for "Drag and Drop".

### Shared Folders

Before you do any of this, make sure you've followed the directions above for installing Guest Additions.

A shared folder can be setup both before launching your Debian (32-bit) instance, and while it's running. For these directions, we'll be doing it before we get the instance running.

In the settings menu for that particular instance, click "Shared Folders", click the little image of the folder with the green + on top of it. The "Folder Path" option determines which folder on your main computer you would like to share with your VirtualBox instance. After you've got that selected, tick the "Auto-mount" option, then start up the Debian instance.

![Shared Folders](https://i.imgur.com/L9CdLed.png)

You might notice you lack permissions to access the shared folder. Type this in your terminal then reboot.

    sudo adduser $USER vboxsf
