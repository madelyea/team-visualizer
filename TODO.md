#TODO:

- Complete EtherTalk wrappers

  - Need to pass socket objects from wrappers

https://stackoverflow.com/questions/6666798/python-passing-socket-by-reference

- json format for sending orders and timestamps (Connor)

- Work with Bart's notes, threading

- If time, build test server

## Possible Issues

### Why is getconf timing out in send\_file?

- Get rid of tqdm in EtherTalk

- What is going on?? Is getconf receiving anything at all?

- Does calling socket.close() in both send\_file() and receive\_file() affect the conf?

  - client\_socket.close() could be closing before conf is really sent, or before getconf can recv()

## On when time windows fail

Anytime the leader or follower's listener is busy for more than the window time, the follower puts nones on and incorrectly sets leader\_response to False, thus setting is\_leader to True.
