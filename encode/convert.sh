for f in *.avi; do
  ffmpeg -i "$f" -c:v libtheora -q:v 9 "${f%.avi}.ogv"
  rm "$f"
done
