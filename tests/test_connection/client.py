# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
#!/usr/bin/env python
# client.py

import pickle

def send_message(payload):
    import socket
    
    TCP_IP = '127.0.0.1'
    TCP_PORT = 7000
    BUFFER_SIZE = 1024
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # s.settimeout(5)
    s.connect((TCP_IP, TCP_PORT))
    package = pickle.dumps(payload)
    s.send(package)
    conf = pickle.loads(s.recv(BUFFER_SIZE))
    if conf == "[+]":
        print("[+] Received conf")
    else:
        print("[x] No conf")
    s.close()
    
    print("received data:", conf)

def send_file():
    import socket
    import tqdm
    import os

    SEPARATOR = "<SEPARATOR>"
    BUFFER_SIZE = 4096

    host = "192.168.0.10"
    port = 5001
    filename = "data.txt"
    filesize = os.path.getsize(filename)

    s = socket.socket()

    print(f"[+] Connecting to {host}:{port}")
    s.connect((host, port))
    print("[+] Connected.")

    s.send(f"{filename}{SEPARATOR}{filesize}".encode())

    progress = tqdm.tqdm(range(filesize), f"Sending {filename}", unit="B", unit_scale=True, unit_divisor=1024)
    with open(filename, "rb") as f:
        for _ in progress:
            bytes_read = f.read(BUFFER_SIZE)
            if not bytes_read:
                break
            s.sendall(bytes_read)
            progress.update(len(bytes_read))
    s.close()

for i in range(1,11):
    MESSAGE = f"Hello, World! {i}"
    send_message(MESSAGE)
send_message('[x]')
#send_file()
