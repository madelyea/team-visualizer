# server.py

import pickle
import time

def output_message():
    import socket
    #TCP_IP = '127.0.0.1'
    TCP_IP = '127.0.0.1'
    TCP_PORT = 7000
    BUFFER_SIZE = 1024

    #s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s = socket.socket()
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
    #s.bind((TCP_IP, TCP_PORT))
    s.bind(('', TCP_PORT))
    print("socket binded to", TCP_PORT)
    s.listen(1)

    def listen():
        conn, addr = s.accept()
        print("Connection address:", addr)
        payload = conn.recv(BUFFER_SIZE)
        if payload:
            payload = pickle.loads(payload)
            conf = pickle.dumps("[+]")
            # time.sleep(3)
            conn.send(conf)  # echo
        conn.close()
        return payload

    data = ""
    while data != "[x]":
        import pdb; pdb.set_trace()
        print(f"data != '[x]': {data != '[x]'}")
        data = listen()
        print("received data:", data)

def receive_file():
    import socket
    import tqdm
    import os

    # reachable on all ips
    SERVER_HOST = "0.0.0.0"
    SERVER_PORT = 5001
    SEPARATOR = "<SEPARATOR>"
    BUFFER_SIZE = 4096

    s = socket.socket()
    s.bind((SERVER_HOST, SERVER_PORT))
    s.listen(5)
    print(f"[*] Listening as {SERVER_HOST}:{SERVER_PORT}")

    client_socket, address = s.accept()
    print(f"[+] {address} is connected.")

    received = client_socket.recv(BUFFER_SIZE).decode()
    filename, filesize = received.split(SEPARATOR)
    filename = os.path.basename(filename)
    filesize = int(filesize)

    progress = tqdm.tqdm(range(filesize), f"Receiving {filename}", unit="B", unit_scale=True, unit_divisor=1024)
    with open(filename, "wb") as f:
        for _ in progress:
            bytes_read = client_socket.recv(BUFFER_SIZE)
            if not bytes_read:
                break
            f.write(bytes_read)
            progress.update(len(bytes_read))
    client_socket.close()
    s.close()

output_message()
#receive_file()
