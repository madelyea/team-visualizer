# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Tests LEADER for EtherTalk
#
# Sources:  https://docs.pytest.org/en/latest/contents.html
#
# ****************************************************

import os
import sys
import time
import pytest
from datetime import datetime
from datetime import timedelta
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + r'/src')
from EtherTalk import EtherTalk
from OrderCode import OrderCode
from MessageMan import MessageMan
from ErrorHandler import ErrorHandler
from constants import *


args = sys.argv


def leader(is_leader, eh, testing=False, order=None, header=None, filename=None, vbs=False):
    # Sending code
    leader = EtherTalk(is_leader, eh, test=testing, verbose=vbs)

    sleep = 1
    print(f"[z] Put leader to sleep for {sleep} second(s)...")
    time.sleep(sleep)

    if filename is not None:
        print("[+] Sending file.")
        leader.send_file(filename)
    elif order is not None:
        print("[+] Sending header.")
        leader.send(order)
    print(f"[+] Close leader status {leader.close()}")
    print()

def test_handshake(is_leader, eh, fname=None, extension=None, testing=False):
    success = []

    leader = EtherTalk(is_leader, eh, test=testing, verbose=True)

    # Give some time to follower so it can scan ips
    sleep = 1
    print(f"[z] Put leader to sleep for {sleep} second(s)...")
    time.sleep(sleep)
    print(f"[!] Let's create a message to send to our follower")

    mm = MessageMan(eh)
    time_start = datetime.now()
    if '-c' in args:
        command = [OrderCode.PLAYLIST, OrderCode.SLOW_DOWN,
                OrderCode.SPEED_UP, OrderCode.LOOP_PLAY,
                OrderCode.RESUME_LIST, OrderCode.RESUME_SPEED,
                OrderCode.PING_RESPONSE, OrderCode.PAUSE_PLAYBACK,
                OrderCode.NEXT_VIDEO, OrderCode.IDLE]
        msuccess1 = []
        for _, c in enumerate(command):
            message_dict = mm.compose_message('', START_TIME, c)
            status = leader.send(message_dict)
            msuccess1.append(status)
            print(f"[!] Status {status}")

    if '-f' in args:
        fsuccess = []
        ms = []
        for i in range(1,6):
            time.sleep(1)
            fsend = fname+str(i+1)+extension
            header = mm.compose_message('', START_TIME, OrderCode.SEND_FILE)
            status = leader.send_file(header, fsend)
            fsuccess.append(status)
        print(f"command/files:\n{ms}")

    if '-c' in args:
        command = [OrderCode.PLAYLIST, OrderCode.SLOW_DOWN,
                OrderCode.SPEED_UP, OrderCode.LOOP_PLAY,
                OrderCode.RESUME_LIST, OrderCode.RESUME_SPEED,
                OrderCode.PING_RESPONSE, OrderCode.PAUSE_PLAYBACK,
                OrderCode.NEXT_VIDEO, OrderCode.IDLE,
                OrderCode.SHUT_DOWN]
        msuccess2 = []
        for _, c in enumerate(command):
            message_dict = mm.compose_message('', START_TIME, c)
            status = leader.send(message_dict)
            msuccess2.append(status)
            print(f"[!] Status {status}")

    elapsed = datetime.now().timestamp()-time_start.timestamp()
    print(f"Success arrays")
    if '-c' in args:
        print(f"{msuccess1}")
    if '-f' in args:
        print(f"{fsuccess}")
    if '-c' in args:
        print(f"{msuccess2}")
    print(f"Finished in {round(elapsed, 4)} s")

def test_handshake_file(is_leader, eh, fname=None, extension=None, testing=False):
        fsend = f"data.txt"
        sleep = 1
        print(f"[z] Put leader to sleep for {sleep} second(s)...")
        fsuccess = []
        time.sleep(sleep)
        mm = MessageMan(eh)
        header = mm.compose_message('', START_TIME, OrderCode.SEND_FILE)
        leader = EtherTalk(is_leader, eh, test=testing, verbose=True)
        status = leader.send_file(header, fsend)
        fsuccess.append(status)

def test_handshake_files_commands(is_leader, eh, fname=None, extension=None, testing=False):
        fsuccess = []
        ms = []
        command1 = [OrderCode.PLAYLIST, OrderCode.SLOW_DOWN,
                    OrderCode.SPEED_UP, OrderCode.LOOP_PLAY,
                    OrderCode.RESUME_LIST]
        mm = MessageMan(eh)
        leader = EtherTalk(is_leader, eh, test=testing, verbose=True)

        for i, x in enumerate(command1):
            message_dict = mm.compose_message('', START_TIME, x)
            print(f"\nTest leader sending {x}")
            status = leader.send(message_dict)
            ms.append(status)

            time.sleep(1)
            fsend = fname+str(i+1)+extension
            timestart = datetime.now()
            header = mm.compose_message('', START_TIME, OrderCode.SEND_FILE)
            status = leader.send_file(header, fsend)
            fsuccess.append(status)
        print(f"Command/files array:\n{ms}")

def main():
    print("\n-- Test: Leader --\n\n")

    eh = ErrorHandler()

    # File constructors
    fromdir = "./test_file/from/"
    f = fromdir + "TESTFILE"
    ext = ".txt"
    is_leader = True

    # Try one file
    if '--one-file' in args:
        test_handshake_file(is_leader, eh, fname=f, extension=ext, testing=True)
        return
    # Try order followed by file, and so on
    elif '-f' in args and '-c' in args:
        test_handshake_files_commands(is_leader, eh, fname=f, extension=ext, testing=True)
        return
    # Try batch of files sandwiched by ordercodes
    elif '-c' in args or '-f' in args:
        test_handshake(is_leader, eh, fname=f, extension=ext, testing=True)
        return

if __name__ == "__main__":
    main()
