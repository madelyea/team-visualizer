# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
#!/usr/bin/env python
# client.py

import pickle
import socket
import tqdm
import os

def send_message(payload):
    
    TCP_IP = '127.0.0.1'
    TCP_PORT = 7000
    BUFFER_SIZE = 1024
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # s.settimeout(5)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)

    s.connect((TCP_IP, TCP_PORT))
    package = pickle.dumps(payload)
    s.send(package)
    conf = pickle.loads(s.recv(BUFFER_SIZE))
    if conf == "[+]":
        print("[+] Received conf")
    else:
        print("[x] No conf")
    s.close()
    
    print("received data:", conf)

def send_file():

    SEPARATOR = "<SEPARATOR>"
    BUFFER_SIZE = 4096

    # host = "192.168.0.10"
    host = "127.0.0.1"
    port = 5001
    filename = "test_file/from/TESTFILE1.txt"
    filesize = os.path.getsize(filename)

    s = socket.socket()
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    #s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)

    print(f"[+] Connecting to {host}:{port}")
    s.connect((host, port))
    print("[+] Connected.")

    s.send(pickle.dumps(f"{filename}{SEPARATOR}{filesize}"))
    # import pdb; pdb.set_trace()

    # progress = tqdm.tqdm(range(filesize), f"Sending {filename}", unit="B", unit_scale=True, unit_divisor=1024)
    # with open(filename, "rb") as f:
    #     for _ in progress:
    #         bytes_read = f.read(BUFFER_SIZE)
    #         if not bytes_read:
    #             break
    #         s.sendall(bytes_read)
    #         progress.update(len(bytes_read))

    f = open(filename, 'rb')
    bytes_sent = s.sendfile(f)
    print(bytes_sent)
    f.close()

    s.close()

# for i in range(1,11):
#     MESSAGE = f"Hello, World! {i}"
#     send_message(MESSAGE)
# send_message('[x]')
send_file()
