# Tutorial: https://youtu.be/IEEhzQoKtQU
import sys

if sys.argv[1] == "1":

    import threading
    import time

    start = time.perf_counter()

    def do_something(s):
        print(f'Sleeping for {s} second(s)...')
        time.sleep(s)
        print('Done Sleeping...')

    threads = []

    for _ in range(10):
        t = threading.Thread(target=do_something, args=[1.5])
        t.start()
        threads.append(t)
    for thread in threads:
        # Wait here until thread terminates
        thread.join()

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start, 2)} second(s)')

elif sys.argv[1] == "2":

    # Thread pool executor

    import concurrent.futures
    import time

    start = time.perf_counter()

    def do_something(s):
        print(f'Sleeping for {s} second(s)...')
        time.sleep(s)
        return f'Done Sleeping...{s}'
    
    with concurrent.futures.ThreadPoolExecutor() as executor:
        # The future object encapsulates execution of function and allows
        # us to check in on it after its been scheduled. We may then check
        # if its running, done, and check result.
        #secs = [2]
        secs = [5, 4, 3, 2, 1]
        results = [executor.submit(do_something, sec) for sec in secs]

        # Returns results as they are completed
        for f in concurrent.futures.as_completed(results):
            print(f.result())
        # f1 = executor.submit(do_something, 1)
        # f2 = executor.submit(do_something, 1)

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start, 2)} second(s)')

elif sys.argv[1] == "3":

    # Thread pool executor

    import concurrent.futures
    import time

    start = time.perf_counter()

    def do_something(s):
        print(f'Sleeping for {s} second(s)...')
        time.sleep(s)
        return f'Done Sleeping...{s}'
    
    with concurrent.futures.ThreadPoolExecutor() as executor:
        # The future object encapsulates execution of function and allows
        # us to check in on it after its been scheduled. We may then check
        # if its running, done, and check result.
        secs = [5, 4, 3, 2, 1]
        # Submit method returns future object, map returns results
        # Map will return results in order they were started
        results = executor.map(do_something, secs)

        # Exceptions are raised when value retrieved from the
        # result's iterator, rather than at runtime
        for result in results:
            print(result)

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start, 2)} second(s)')

elif sys.argv[1] == "4":

    import concurrent.futures
    import urllib.request
    
    URLS = ['http://www.foxnews.com/',
            'http://www.cnn.com/',
            'http://europe.wsj.com/',
            'http://www.bbc.co.uk/',
            'http://some-made-up-domain.com/']
    
    # Retrieve a single page and report the URL and contents
    def load_url(url, timeout):
        with urllib.request.urlopen(url, timeout=timeout) as conn:
            return conn.read()
    
    # We can use a with statement to ensure threads are cleaned up promptly
    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        # Start the load operations and mark each future with its URL
        future_to_url = {executor.submit(load_url, url, 60): url for url in URLS}
        for future in concurrent.futures.as_completed(future_to_url):
            url = future_to_url[future]
            try:
                data = future.result()
            except Exception as exc:
                print('%r generated an exception: %s' % (url, exc))
            else:
                print('%r page is %d bytes' % (url, len(data)))
