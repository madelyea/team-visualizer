# Threading

## These need threads:

Projector, listener, connection (waiting for input)

#

## Server (Follower)

input listener needs a thread

leader spawns thread for user input

### PiMan

1. PiMan spawns new python thread to run EtherTalk, then sets aside.

### Server Thread

2. Server calls listen. Server thread blocked and listening. On connect:

    - Get new file descriptor for connection, make new thread (client thread), write what it needs to write, and read

#### Client Thread

Client connects to address and port

#

## Notes

Threads exist as subset of a process.

Multiple threads within a process share process state as well as memory and other resources.

Threads share address space.

### Advantages related to our project: 

- Responsiveness: in a one-thread program, if the main execution thread blocks on a long-running task, the entire application can appear to freeze. By moving such long-running tasks to a ***worker thread*** that runs concurrently with the main execution thread, it is possible for the application to remain responsive to user input while executing tasks in the background. On the other hand, in most cases multithreading is not the only way to keep a program responsive, with ***non-blocking I/O and/or Unix signals*** being available for gaining similar results.

- Lower resource consumption: using threads, an application can serve multiple clients concurrently using fewer resources than it would need when using multiple process copies of itself. For example, the Apache HTTP server uses ***thread pools***: a pool of listener threads for listening to incoming requests, and a ***pool of server threads*** for processing those requests.

- Better system utilization: as an example, a file system using multiple threads can achieve higher throughput and lower latency since data in a faster medium (such as cache memory) can be retrieved by one thread while another thread retrieves data from a slower medium (such as external storage) with neither thread waiting for the other to finish.

- Simplified sharing and communication: unlike processes, which require a message passing or shared memory mechanism to perform ***inter-process communication (IPC)***, threads can communicate through data, code and files they already share.

### Drawbacks:

- Synchronization: since threads share the same address space, the programmer must be **careful to avoid race conditions and other non-intuitive behaviors**. In order for data to be correctly manipulated, **threads will often need to rendezvous in time in order to process the data in the correct order**. Threads may also require mutually exclusive operations (often implemented using ***mutexes***) to prevent common data from being read or overwritten in one thread while being modified by another. **Careless use of such primitives can lead to deadlocks, livelocks or races over resources**.

- **Thread crashes a process**: an illegal operation performed by a thread crashes the entire process; therefore, **one misbehaving thread can disrupt the processing of all the other threads in the application**.