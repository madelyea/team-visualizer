# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Tests for EtherTalk. Spawn a LEADER and FOLLOWER.
#
# Sources:  https://docs.pytest.org/en/latest/contents.html
#
# ****************************************************

import os
import sys
import time
import pytest
import concurrent.futures
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + r'/src')
from EtherTalk import EtherTalk
from OrderCode import OrderCode
from MessageMan import MessageMan


def test_gethostname():
    assert 1 == 1

def test_receive():
    assert 1 == 1

def test_send(verbose, file):
    assert 1 == 1

def runFollower(testing=True, todir='.test_file/to/', delete=True):
    print("\n-- Test: Follower --\n\n")
    follower(testing=testing, topath=todir, delete=delete)

def follower(testing=True, topath='.test_file/to/', delete=True):
    # Sending code
    follower = EtherTalk(is_leader=False, test=testing, verbose=False)
    print(f"[&] Follower at {hex(id(follower))}")
    # Send file
    code = 10
    while code != OrderCode.SHUT_DOWN:
        # Recieve status and header to display and delete sent in files
        status, header = follower.listen(path=topath)
        # Get time and code from header
        code = header['code']
        start = header['time']
        end = time.time()
        print(f"Sent in {round((end-start)*1000, 2)}ms")
        # If we got a file, confirm its presence, then delete
        if code == OrderCode.SEND_FILE:
            # Strip directory path
            filename = os.path.basename(header['filename'])
            # Confirm that file's existence
            found = " " if os.path.isfile(topath + filename) else " NOT "
            print(f"[+] {topath + filename} was{found}found.")
            # Clean up file that was created
            if delete == True:
                os.remove(topath + filename)
        print(f"[!] Code {code}")
        print()
    print(f"[+] Status {status}")

def runLeader(testing=True):
    print("\n-- Test: Leader --\n\n")
    # File constructors
    fromdir = "./test_file/from/"
    f = fromdir + "TESTFILE"
    ext = ".txt"
    # Send 5 test files then send shutdown code
    for i in range(1,6):
        filename = (f + str(i) + ext)
        leader(testing=testing, file=filename, vbs=False)
    leader(OrderCode.SHUT_DOWN)

def leader(testing=True, order=None, header=None, file=None, vbs=False):
    # Sending code
    leader = EtherTalk(is_leader=True, test=testing, verbose=vbs)
    print(f"[&] Leader at {hex(id(leader))}")
    sleep = 1
    print(f"[z] Put leader to sleep for {sleep} second(s)...")
    time.sleep(sleep)

    if file is not None:
        print("[+] Sending file.")
        leader.send_file(file)
    elif order is not None:
        print("[+] Sending header.")
        leader.send(order)
    print(f"[+] Close leader, status {leader.close()}")
    print()


def main():
    print("Test: generate playlist and send")
    
    # verbose = None
    # while verbose not in [True, False]:
    #     print("Verbose output? (y/n)")
    #     res = input(" -> ")
    #     print(f"You replied with '{res}'.")
    #     verbose = True if res == 'y' else False if res == 'n' else None
    #     print(f"You replied with '{verbose}'.")

    # File construtors
    follower_write_path = "./test_file/to/"

    # Spawn two threads
    start = time.perf_counter()
    with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.submit(runFollower, testing=True,
                        todir=follower_write_path, delete=False)
        executor.submit(runLeader, testing=True)
    finish = time.perf_counter()
    print(f'Finished threads in {round(finish-start, 2)} second(s)')

if __name__ == "__main__":
    main()
