# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Tests for ErrorHandler
#
# Sources:  https://docs.pytest.org/en/latest/contents.html
#
# ****************************************************

import os
import sys
import pytest
import logging
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + r'/src')

from ErrorHandler import ErrorHandler

class TestErrorHandler:
    def test_remove_root_handlers(self):
        eh = ErrorHandler()
        try:
            raise ValueError("Log Error Test Handler")
        except Exception as e:
            eh.log_error(e)
            
        assert logging.root.hasHandlers() == True
        
        eh.remove_root_handlers()
        assert logging.root.hasHandlers() == False
    
    def test_log_error(self):
        eh = ErrorHandler()
        try:
            raise ValueError("Log Error Test")
        except Exception as e:
            eh.log_error(e)
        
        assert os.path.isfile(eh.log_file) == True
    
    def test_log_warning(self):
        eh = ErrorHandler()
        try:
            raise ValueError("Log Warning Test")
        except Exception as e:
            eh.log_error(e)
        
        assert os.path.isfile(eh.log_file) == True
    
    def test_log_info(self):
        eh = ErrorHandler()
        try:
            raise ValueError("Log Info Test")
        except Exception as e:
            eh.log_error(e)
        
        assert os.path.isfile(eh.log_file) == True
    
    def test_log_critical(self):
        eh = ErrorHandler()
        try:
            raise ValueError("Log Critical Test")
        except Exception as e:
            eh.log_error(e)
        
        assert os.path.isfile(eh.log_file) == True
