# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Test FOLLOWER for EtherTalk
#
# Sources:  https://docs.pytest.org/en/latest/contents.html
#
# ****************************************************

import os
import sys
import time
import pytest
from datetime import datetime
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + r'/src')
from EtherTalk import EtherTalk
from OrderCode import OrderCode
from ErrorHandler import ErrorHandler
from constants import *


args = sys.argv


def follower(is_leader, topath):
    # Sending code
    follower = EtherTalk(is_leader, test=False, verbose=False)

    code = 10
    while code != OrderCode.SHUT_DOWN:
        print()
        # Recieve status and header to display and delete sent in files
        status, header = follower.listen(path=topath)
        # Get time and code from header
        code = header['code']
        start = header['time']
        end = datetime.now().timestamp()
        print(f"Sent in {round((end-start)*1000, 2)}ms")

        # If we got a file, confirm its presence, then delete
        if code == OrderCode.SEND_FILE:
            # Strip directory path
            filename = os.path.basename(header['filename'])
            # Confirm that file's existence
            found = " " if os.path.isfile(topath + filename) else " NOT "
            print(f"{topath + filename} was{found}found.")
            # Clean up file that was created
            #os.remove(topath + filename)
        print(f"[!] Code {code}")

    print(f"[+] Status {status}")

def test_handshake(is_leader, eh, topath, testing=False):
    follower = EtherTalk(is_leader, error_handler=eh, test=testing, verbose=True)
    code = 10
    while code != OrderCode.SHUT_DOWN:
        status, header = follower.listen(topath)
        print(f"[!] Status {status}\n[!] Header {header}")
        # Recieve status and header to display and delete sent in files
        # Get time and code from header
        code = header['code']

        # If we got a file, confirm its presence, then delete
        if code == OrderCode.SEND_FILE:
            # Strip directory path
            filename = os.path.basename(header['file'])
            # Confirm that file's existence and size is same as file sent in
            found = " " if os.path.isfile(topath + filename) else " NOT "
            fsizediff = header['filesize'] - os.stat(topath+filename).st_size
            print(f"{topath + filename} was{found}found.")
            if fsizediff == 0:
                print(f"[+] Files are exact!")
            else:
                print(f"[x] fsizediff: {fsizediff}")
            # Clean up file that was created
            if '-d' in args:
                os.remove(topath + filename)

def main():
    print("\n-- Test: Follower --\n\n")

    eh = ErrorHandler()

    # File construtors
    todir = "./test_file/to/"

    # Test handshake
    test_handshake(False, eh, todir, testing=True)
    # Test receiving files
    # follower(todir)


if __name__ == "__main__":
    main()
