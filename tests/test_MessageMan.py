# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Tests for MessageMan
#
# Sources:  https://docs.pytest.org/en/latest/contents.html
#
# ****************************************************

import os
import sys
import pytest
from datetime import datetime
from datetime import timedelta
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + r'/src')

from MessageMan import MessageMan
from ErrorHandler import ErrorHandler
from OrderCode import OrderCode

class TestMessageMan:
    def test_parse_message(self):
        eh = ErrorHandler()
        mm = MessageMan(eh)
        assert 1 == 1
    
    def test_compose_message_correctly(self):
        eh = ErrorHandler()
        mm = MessageMan(eh)
        
        now = datetime.now()
        order = OrderCode.NONE
        message_string = ""
        result = mm.compose_message(message_string, now, order) 
        
        message = {
                    "code"       : (order.value),
                    "sent_time"  : (now.timestamp()),
                    "start_time" : (now.timestamp()),
                    "message"    : (message_string)
                }
        
        assert result["code"] == message["code"]
        assert result["message"] == message["message"]
        assert result["start_time"] == message["start_time"]
        
    def test_compose_message_incorrectly(self):
        eh = ErrorHandler()
        mm = MessageMan(eh)
        
        now = datetime.now()
        order = OrderCode.NONE
        message_string = ''
        result = mm.compose_message(message_string, now, order) 
        
        message = {
                    "code"       : (OrderCode.PLAYLIST.value),
                    "sent_time"  : (now.timestamp()),
                    "start_time" : ((datetime.now() + timedelta(seconds=2)).timestamp()),
                    "message"    : ('PIE!')
                }
        
        assert result["code"] != message["code"]
        assert result["message"] != message["message"]
        assert result["start_time"] != message["start_time"]
