# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Tests for Device
#
# Sources:  https://docs.pytest.org/en/latest/contents.html
#
# ****************************************************

import os
import sys
import pytest
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + r'/src')

from Device import Device

class TestDevice:
    def test_has_found_device(self):
        """ Test to check to see if we have any type of device on the pi.

        Args:
            No Args
        Returns:
            Nothing
        """
        df = 'Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub\nBus 001 Device 004: ID 046d:c52b Logitech, Inc. Unifying Receiver\n'
        result = Device().search(df)
        assert result == True

    def test_has_not_found_device(self):
        """ Test to check to see if we have no type of device on the pi.

        Args:
            No Args
        Returns:
            Nothing
        """
        df = 'Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub\n'
        result = Device().search(df)
        assert result == False
