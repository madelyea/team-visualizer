# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Sratch space
#
# Sources:  https://docs.pytest.org/en/latest/contents.html
#
# ****************************************************

import os
import sys
import time
import json
import pytest
import pickle
from socket import *
import netifaces as ni
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + r'/src')
# import VideoMan as vm
import EtherTalk as EtherTalk
from OrderCode import *


# print(OrderCode.SHUT_DOWN.value != 1)
# print(OrderCode.SHUT_DOWN != 1)
# print(OrderCode.SHUT_DOWN.value == 1)
# print(OrderCode.SHUT_DOWN == 1)
print(type(OrderCode.SHUT_DOWN))
print(type(OrderCode))


# Class private access
# class myclass:
#     def __init__(self):
#             self.myvar = "private variable"
#     def __private(self):
#             print(self.myvar + ". you shouldn't be able to see me...")
#     def public(self):
#             print(self.myvar + ". but it's ok, you used my public method.")
# instance = myclass()
# instance._myclass__private()
# instance.public()
# instance.myvar = "HEY, that's illegal..."
# instance._myclass__private()
# instance.public()

help(EtherTalk)