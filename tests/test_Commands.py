# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Tests for Commands
#
# Sources:  https://docs.pytest.org/en/latest/contents.html
#
# ****************************************************

import os
import sys
import pytest
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + r'/src')

from Commands import Commands
from OrderCode import *
from datetime import datetime
from MessageMan import MessageMan
import Commands as c_queue

class TestDevice:
    def test_enqueue(self):
        """ Test to check to see if we have any type of device on the pi.

        Args:
            No Args
        Returns:
            Nothing
        """
        c_queue.init()
        header = MessageMan().compose_message("", datetime.now(), OrderCode.NONE)
        
        # Remove initial header on creation
        trash = Commands().dequeue()
        
        Commands().enqueue(header)
        assert c_queue.CommandQueue.empty() == False
        
    def test_dequeue(self):
        """ Test to check to see if we have any type of device on the pi.

        Args:
            No Args
        Returns:
            Nothing
        """
        c_queue.init()
        header = MessageMan().compose_message("", datetime.now(), OrderCode.NONE)
        
        # Remove initial header on creation
        trash = Commands().dequeue()
        
        Commands().enqueue(header)
        result = Commands().dequeue()
        assert result == header
        
    def test_flush_queue(self):
        """ Test to check to see if we have any type of device on the pi.

        Args:
            No Args
        Returns:
            Nothing
        """
        c_queue.init()
        header = MessageMan().compose_message("", datetime.now(), OrderCode.NONE)
        Commands().enqueue(header)
        Commands().flush_queue()
        assert c_queue.CommandQueue.empty() == True
        
    
        
