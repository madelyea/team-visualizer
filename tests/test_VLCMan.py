# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Tests for VLCMan
#
# Sources:  https://docs.pytest.org/en/latest/contents.html
#
# ****************************************************

import os
import sys
import pytest
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + r'/src')
from VLCMan import VLCMan
from ErrorHandler import ErrorHandler
from constants import *

class TestVLCMan:
    def test_start_vlc(self):
        eh = ErrorHandler()
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        assert vlc.start_vlc() == True
        
    def test_get_length(self):
        eh = ErrorHandler()
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        vlc.start_vlc()
        assert vlc.get_length("") == 0.0
    
    def test_get_length_of_current(self):
        eh = ErrorHandler()
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        vlc.start_vlc()
        assert vlc.get_length_of_current() >= 0.0
        
    def test_get_position(self):
        eh = ErrorHandler()
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        vlc.start_vlc()
        assert vlc.get_position() >= 0.0
        
    def test_set_position(self):
        eh = ErrorHandler()
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        vlc.start_vlc()
        assert vlc.set_position(vlc.get_id(), 2000000.0) == True
        assert vlc.get_position() == 2.0
        
    def test_get_rate(self):
        eh = ErrorHandler()
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        vlc.start_vlc()
        assert vlc.get_rate() == 1.0
        
    # Hmm
    def test_get_id(self):
        eh = ErrorHandler()
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        #vlc.start_vlc()
        #assert vlc.get_id == str()
        
    def test_randomize_videos(self):
        eh = ErrorHandler()
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        vlc.start_vlc()
        
        assert vlc.randomize_videos() == True
        
    def test_toggle_loop_video(self):
        eh = ErrorHandler()
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        vlc.start_vlc()
        vlc.loop_status = "Playlist"
        vlc.toggle_loop_video()
        
        assert vlc.loop_status == "Track"
        
        vlc.toggle_loop_video()
        
        assert vlc.loop_status == "Playlist"
        
    def test_set_loop_video(self):
        eh = ErrorHandler()
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        vlc.start_vlc()
        vlc.loop_status = "Playlist"
        vlc.set_loop_video()
        
        assert vlc.loop_status == "Track"
        
    def test_set_loop_playlist(self):
        eh = ErrorHandler()
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        vlc.start_vlc()
        vlc.loop_status = "Track"
        vlc.set_loop_playlist()
        
        assert vlc.loop_status == "Playlist"
        
    def test_set_rate(self):
        eh = ErrorHandler()
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        vlc.start_vlc()
        vlc.set_rate(5)
        assert vlc.get_rate() == 6.0
        
    def test_reset_rate(self):
        eh = ErrorHandler()
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        vlc.start_vlc()
        vlc.reset_rate()
        assert vlc.get_rate() == 1.0
        
    def test_create_playlist(self):
        eh = ErrorHandler()
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        try:
            os.remove(PLAYLIST_NAME)
        except:
            pass
            
        assert vlc.create_playlist() == True
        
        # Just testing to see if the playlist is there
        assert os.path.isfile(PLAYLIST_NAME) == True