# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Tests for PiMan
#
# Sources:  https://docs.pytest.org/en/latest/contents.html
#
# ****************************************************

import os
import sys
import pytest
from datetime import datetime
from datetime import timedelta
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + r'/src')
from constants import *
from OrderCode import *
from PiMan import PiMan
from ErrorHandler import ErrorHandler
from EtherTalk import EtherTalk
from MessageMan import MessageMan
from VLCMan import VLCMan
from Device import Device
from Commands import Commands
import Commands as c_queue

class TestPiMan:
    # I want to test the shutdown logic on this loop.
    # but then the test would shutdown the pi.
    def waiting_loop(self):
        eh = ErrorHandler()
        pi = PiMan(eh)
        assert 1 == 1
    
    def test_get_started_leader(self):
        c_queue.init()
        eh = ErrorHandler()
        pi = PiMan(eh)
        et = EtherTalk(error_handler=eh, verbose=False)
        mm = MessageMan(error_handler=eh)
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        device = True
        
        # see if we came back safely.
        assert pi.get_started(vlc, et, mm, device) == True
        # see if that playlist is there.
        assert os.path.isfile(PLAYLIST_NAME) == True
        # We have a device so we should be leader
        assert c_queue.IsLeader == True
            
    def test_get_started_follower(self):
        c_queue.init()
        eh = ErrorHandler()
        pi = PiMan(eh)
        et = EtherTalk(error_handler=eh, verbose=False)
        mm = MessageMan(error_handler=eh)
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        device = False
        
        # see if we came back safely.
        assert pi.get_started(vlc, et, mm, device) == True
        # see if that playlist is not there.
        assert os.path.isfile(PLAYLIST_NAME) == False

        # I need to find out how to dummy up et.ping for this
        if et.ping() == 0:
            assert c_queue.IsLeader == False
        else:
            assert c_queue.IsLeader == True
    
    # I should prob set it up to pass in a time and check...
    def test_check_time(self):
        eh = ErrorHandler()
        pi = PiMan(eh)
    
        hour = datetime.now().hour
        if hour >= SEVEN_AM and hour < SEVEN_PM:
            assert pi.check_time() == False
        else:
            assert pi.check_time() == True
    
    def test_exec_command(self):
        eh = ErrorHandler()
        pi = PiMan(eh)
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        time_start = datetime.now()
        
        # Just testing to see if the results come back correctly for now.
        assert pi.exec_command(vlc, time_start, OrderCode.SHUT_DOWN) == True
        assert pi.exec_command(vlc, time_start, OrderCode.PLAYLIST) == True
        assert pi.exec_command(vlc, time_start, OrderCode.SLOW_DOWN) == True
        assert pi.exec_command(vlc, time_start, OrderCode.SPEED_UP) == True
        assert pi.exec_command(vlc, time_start, OrderCode.LOOP_PLAY) == True
        assert pi.exec_command(vlc, time_start, OrderCode.RESUME_LIST) == True
        assert pi.exec_command(vlc, time_start, OrderCode.RESUME_SPEED) == True
        assert pi.exec_command(vlc, time_start, OrderCode.PAUSE_PLAYBACK) == True
        assert pi.exec_command(vlc, time_start, OrderCode.NEXT_VIDEO) == True
        assert pi.exec_command(vlc, time_start, OrderCode.IDLE) == True
        assert pi.exec_command(vlc, time_start, OrderCode.NONE) == False
    
    def test_create_playlist(self):
        eh = ErrorHandler()
        pi = PiMan(eh)
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        try:
            os.remove(PLAYLIST_NAME)
        except:
            pass
        
        pi.create_playlist(vlc)
        
        # Just testing to see if the playlist is there
        assert os.path.isfile(PLAYLIST_NAME) == True
    
    def test_send_orders(self):
        eh = ErrorHandler()
        pi = PiMan(eh)
        et = EtherTalk(error_handler=eh, verbose=False)
        mm = MessageMan(error_handler=eh)
        
        message_dict = mm.compose_message("", START_TIME, OrderCode.NONE)
        
        # Need to find a way to dummy up et to get a more predictable test.
        if et.send(message_dict) == 0:
            assert pi.send_orders("", et, mm, START_TIME, OrderCode.NONE) == True
        else:
            assert pi.send_orders("", et, mm, START_TIME, OrderCode.NONE) == False
    
    def test_next_command_become_follower(self):
        c_queue.init()
        eh = ErrorHandler()
        pi = PiMan(eh)
        et = EtherTalk(error_handler=eh, verbose=False)
        mm = MessageMan(error_handler=eh)
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        device = False
        
        # We need to wait until we are in the 3 second window.
        while datetime.now().second > 2:
            pass
        
        pi.next_command(vlc, et, mm, device)
        
        # Need to figure out how to dummy et.ping()
        if et.ping() == 0:
            assert c_queue.IsLeader == False
            assert c_queue.CommandQueue.empty() == True
        else:
            assert c_queue.IsLeader == True
            assert c_queue.CommandQueue.empty() == False
        
    def test_next_command_queue_not_empty(self):
        c_queue.init()
        eh = ErrorHandler()
        pi = PiMan(eh)
        et = EtherTalk(error_handler=eh, verbose=False)
        mm = MessageMan(error_handler=eh)
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        device = True
        pi.leader_response_time = datetime.now()
        
        # Remove initial header on creation
        trash = Commands().dequeue()
        
        header = mm.compose_message("", START_TIME, OrderCode.SHUT_DOWN)
        Commands().enqueue(header)
        
        pi.next_command(vlc, et, mm, device)
        
        # is_shutdown_time should be True if the command was executed
        assert pi.is_shutdown_time == True
        # another test. this should be None if the command was executed
        assert pi.leader_response_time is None
        
    def test_next_command_idle(self):
        c_queue.init()
        eh = ErrorHandler()
        pi = PiMan(eh)
        et = EtherTalk(error_handler=eh, verbose=False)
        mm = MessageMan(error_handler=eh)
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        device = True
        pi.leader_response_time = datetime.now()
        pi.timestamp = datetime.now() - timedelta(days=IDLE_SEND_TIME)

        # Remove initial header on creation
        trash = Commands().dequeue()

        pi.next_command(vlc, et, mm, device)
        
        # This should be None if the command was executed
        assert pi.leader_response_time is None
    
    # This one could prove difficult. Idk how to test the creation of another thread and what
    # happens on it :(
    def test_listener(self):
        eh = ErrorHandler()
        pi = PiMan(eh)
        assert 1 == 1
    
    def next_command_follower_execute_command_correctly(self):
        c_queue.init()
        eh = ErrorHandler()
        pi = PiMan(eh)
        mm = MessageMan(error_handler=eh)
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        pi.next_command_follower(mm, vlc)
        # assert the time is still not set and the queue was taken off to be used.
        assert c_queue.CommandQueue.empty() == True
        assert pi.leader_response_time is None
        
    def next_command_follower_set_leader_response_time_correctly(self):
        c_queue.init()
        eh = ErrorHandler()
        pi = PiMan(eh)
        mm = MessageMan(error_handler=eh)
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        # Queue needs to be empty to set leader_response_time
        trash = Commands().dequeue()
        
        pi.next_command_follower(mm, vlc)
        # assert we have a time and the queue is still empty
        assert pi.leader_response_time is not None
        assert c_queue.CommandQueue.empty() == True
        
    def next_command_follower_take_over_leadership(self):
        c_queue.init()
        eh = ErrorHandler()
        pi = PiMan(eh)
        mm = MessageMan(error_handler=eh)
        vlc = VLCMan(VIDEOS_PATH, PLAYLIST_NAME, "", error_handler=eh)
        
        pi.leader_response_time = datetime.now()
        
        pi.next_command_follower(mm, vlc)
        # We should be the leader now and with no queue and the leader_response_time reset
        assert c_queue.CommandQueue.empty() == True
        assert pi.leader_response_time is None
        assert c_queue.IsLeader == True