# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Tests for UserInput
#
# Sources:  https://docs.pytest.org/en/latest/contents.html
#
# ****************************************************

import os
import sys
import pytest
from pynput import mouse
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + r'/src')
from UserInput import UserInput
from OrderCode import *
from Commands import Commands
import Commands as c_queue

class TestUserInput:
    def test_on_click_left(self):
        c_queue.init()
        ui = UserInput()
        c_queue.IsLeader = True
        
        # Remove initial header on creation
        trash = Commands().dequeue()
        
        ui.on_click(1, 1, mouse.Button.left, False)
        
        result = Commands().dequeue()
        
        assert OrderCode(result['code']) == OrderCode.NEXT_VIDEO

    def test_on_click_right(self):
        c_queue.init()
        ui = UserInput()
        c_queue.IsLeader = True
        
        # Remove initial header on creation
        trash = Commands().dequeue()
        
        ui.on_click(1, 1, mouse.Button.right, False)
        
        result = Commands().dequeue()
        
        assert OrderCode(result['code']) == OrderCode.LOOP_PLAY
        
    def test_on_click_middle(self):
        c_queue.init()
        ui = UserInput()
        c_queue.IsLeader = True
        
        # Remove initial header on creation
        trash = Commands().dequeue()
        
        ui.on_click(1, 1, mouse.Button.middle, False)
        
        result = Commands().dequeue()
        
        assert OrderCode(result['code']) == OrderCode.RESUME_SPEED
    
    def test_on_scroll_fast(self):
        c_queue.init()
        ui = UserInput()
        c_queue.IsLeader = True
        
        # Remove initial header on creation
        trash = Commands().dequeue()
        
        ui.on_scroll(1, 1, 1, 1)
        
        result = Commands().dequeue()
        
        assert OrderCode(result['code']) == OrderCode.SPEED_UP

    def test_on_scroll_slow(self):
        c_queue.init()
        ui = UserInput()
        c_queue.IsLeader = True
        
        # Remove initial header on creation
        trash = Commands().dequeue()
        
        ui.on_scroll(1, 1, 1, -1)
        
        result = Commands().dequeue()
        
        assert OrderCode(result['code']) == OrderCode.SLOW_DOWN
    
    def test_on_move(self):
        ui = UserInput()
        ui.on_move(1, 1)
        c_queue.IsLeader = True
        
        assert ui.mouse_controller.position == (0, 0)