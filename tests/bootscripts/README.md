# How to start VLC on boot

Using this method: https://www.raspberrypi-spy.co.uk/2014/05/how-to-autostart-apps-in-rasbian-lxde-desktop/

1. Create/edit shell script (`vlc-start.py`):

2. Create/edit config file ~/.config/lxsession/LXDE/autostart and add vlc-start.py:

    `@/usr/bin/python /home/pi/Projects/team-visualizer/bootscripts/vlc-start.py`

  - This autostart file starts all processes that handle window management. If you want a fully usable desktop, you must add (before the vlc start script above):
  
    @lxpanel --profile LXDE-pi
    @pcmanfm --desktop --profile LXDE-pi
    @xscreensaver -no-splash

Or just copy the file `autostart` into ~/.config/lxsession/LXDE/autostart.

## Debug notes

### Resources
https://www.dexterindustries.com/howto/run-a-program-on-your-raspberry-pi-at-startup/

https://linuxconfig.org/how-to-automatically-execute-shell-script-at-startup-boot-on-systemd-linux

### Method
https://www.raspberrypi.org/forums/viewtopic.php?t=17051

- Actually works but two instances start, one in the foreground, one in back

https://www.raspberrypi-spy.co.uk/2014/05/how-to-autostart-apps-in-rasbian-lxde-desktop/

- Trying 'Auto-run Python Scripts' with 'Method 1'
  - Still starts 2 processes

- Method 2

https://www.raspberrypi.org/forums/viewtopic.php?t=59285

- Above fixed 2 instance problem
