# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose: To check for a device plugged into our pi. This will check for
#          any device that has a driver other than 'hub' or 'Hub'
#
# Sources:  https://stackoverflow.com/a/8265634
# ****************************************************

import re
import subprocess

class Device:
    def __init__(self):
        self.df = subprocess.check_output("lsusb").decode('utf-8')
        self.has_device = self.search(self.df)

    def search(self, df) -> bool:
        """ Checks to see if we have any type of device on the pi.

        Args:
            None

        Returns:
            True if we have found any type of device that is not a 'hub'. False if we did not.
        """
        device_re = re.compile(r"Bus\s+(?P<bus>\d+)\s+Device\s+(?P<device>\d+).+ID\s(?P<id>\w+:\w+)\s(?P<tag>.+)\s(?P<driver>.+)$", re.I)
        devices = []
        for i in df.split('\n'):
            if i:
                info = device_re.match(i)
                if info:
                    dinfo = info.groupdict()
                    if str.lower(dinfo['driver']) != 'hub':
                        devices.append(dinfo)
                        
        if not devices:
            return False
        else:
            return True
        
