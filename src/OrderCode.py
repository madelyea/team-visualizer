# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Lists the command codes and their orders in an IntEnum list, for communication
# between RasPi units
#
# ****************************************************

from enum import IntEnum


class OrderCode(IntEnum):
    """An IntEnum class that holds the command codes for communication between
    RasPi units \n
        `SHUT_DOWN = 1`
        `PLAYLIST = 2`
        `SLOW_DOWN = 3`
        `SPEED_UP = 4`
        `LOOP_PLAY = 5`
        `RESUME_LIST = 6`
        `RESUME_SPEED = 7`
        `PING_RESPONSE = 8`
        `PAUSE_PLAYBACK = 9`
        `NEXT_VIDEO = 10`
        `SEND_FILE = 11`
        `IDLE = 20`
        `USURP = 66`
        `FAILED = 77`
        `NONE = 99`

    Examples:

    `import OrderCode as OrderCode`

    `print(OrderCode.SHUT_DOWN)`
    `>> OrderCode.SHUT_DOWN`

    `print(OrderCode.SHUT_DOWN.value)`
    `>> 1`

    `print(OrderCode(1))`
    `>> OrderCode.SHUT_DOWN`
    """
    SHUT_DOWN = 1       # Command or response to begin shutdown procedures
    PLAYLIST = 2        # Indicates new playlist is contained in the message
    SLOW_DOWN = 3       # Slowdown playback of videos
    SPEED_UP = 4        # Speedup playback of videos
    LOOP_PLAY = 5       # Remain on current video or visual indefinitely
    RESUME_LIST = 6     # Return to normal playback mode
    RESUME_SPEED = 7    # Resume normal playback speed of videos
    PING_RESPONSE = 8   # For sending and receiving a response of connection
    PAUSE_PLAYBACK = 9  # Command to suspend any visual projections
    NEXT_VIDEO = 10     # Next video
    SEND_FILE = 11      # Send file
    IDLE = 20           # Idle
    USURP = 66          # Tell other RasPi "I am the leader now"
    FAILED = 77         # Something failed
    NONE = 99           # None
