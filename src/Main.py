# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Starts the visualizer program by loading through PiMan
#
# ****************************************************
import os
from PiMan import PiMan
from ErrorHandler import ErrorHandler


def main():
    eh = ErrorHandler()
    pi = PiMan(eh)
    os.system("sudo hwclock -s")
    try:
        if not pi.waiting_loop():
            eh.log_warning(
                "PiMan WaitingLoop() returned False, did not close resources properly.")
    except Exception as e:
        eh.log_critical(
            "PiMan could not properly start. Major startup error.")
        subprocess.Popen("sudo shutdown -P now", shell=True)


if __name__ == '__main__':
    main()
