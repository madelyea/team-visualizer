# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Contains any constant values used for all program classes
#
# ****************************************************

# Time in minutes that a unit should wait before declaring a connection lost
TIMEOUT_DURATION = 2

# Number of seconds delay before both RasPis should execute an order
START_TIME = 2
# Start time for playlist, should be proportional to playlist len
PLAYLIST_START_TIME = 5

# Number of minutes the follower should wait before taking over as leader
FOLLOWER_TAKEOVER_WAIT = 3

# Number of seconds between sends of "Idle" messages from leader
IDLE_SEND_TIME = 10

# Set Times for the CheckTime() in PiMan
SEVEN_AM = 7
SEVEN_PM = 19

# Constants for EtherTalk transfers
HEADER = "<HEADER>"  # Delimiter
BUFFER_SIZE = 4096
# Time in seconds that a sender should wait to receive confirmation
CONF_TIMEOUT = 5

# Path to the video files
VIDEOS_PATH = "/home/pi/Videos/"
# "Working" is for testing, needs forward slash at end
#VIDEOS_PATH = "/home/pi/Projects/Working/"
# Path to the Playlist.m3u
WRITE_PATH = "/home/pi/Projects/team-visualizer/src/"

# Name of the playlist file
PLAYLIST_NAME = "/home/pi/Projects/team-visualizer/src/playlist.m3u"
# Max duration in seconds before needing to be trimmed
MAX_DURATION = 1800
# Amount in seconds to trim off of long videos
TRIM_AMOUNT = "600"

# Known pi addresses
KNOWN_IPS = ["169.254.200.133",
             "169.254.213.164",
             "169.254.168.216"]
            #  "127.0.0.1"]

# Queue constants
MAX_QUEUE_SIZE = 3

# rate bounds
RATE_MAX = 10.0
RATE_MIN = 0.1
