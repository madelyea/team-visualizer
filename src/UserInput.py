# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Receive input from user to be passed up to InputOutputHandler
# Sources:
# ****************************************************

from Commands import Commands
from pynput import mouse
import time
from OrderCode import *
from MessageMan import MessageMan
from datetime import datetime
from datetime import timedelta
from constants import *

import Commands as c_queue
c_queue.init()


class UserInput:

    def __init__(self, error_handler=None):
        self.left_hold = 0.0
        self.right_hold = 0.0
        self.middle_hold = 0.0
        self.mouse_controller = mouse.Controller()
        self.eh = error_handler

    def on_click(self, x, y, button, pressed):
        """ Deals with mouse button input.

        Args:
            x (int): x location of cursor when clicked.
            y (int): y location of cursor when clicked.
            button (Mouse.Button): imported from pynput.mouse
            pressed (bool): If True, mouse button pressed. If False, mouse button released.

        Returns:
            bool: The return value. If True, continue listening. If False, stop listening.
        """
        if c_queue.IsLeader == False:
            raise
        
        if pressed:
            if button == mouse.Button.left:
                self.left_hold = time.time()
                print("Left mouse button pressed at", self.left_hold)
            elif button == mouse.Button.right:
                self.right_hold = time.time()
                print("Right mouse button pressed at", self.left_hold)
            elif button == mouse.Button.middle:
                self.middle_hold = time.time()
                print("Middle mouse button pressed at", self.middle_hold)

        if not pressed:
            end_time = time.time()
            if button == mouse.Button.left:
                elapsed = (end_time - self.left_hold) * 1000
                print("Left mouse button released at",
                      end_time, "after", elapsed, "ms")
                header = MessageMan(self.eh).compose_message("", 0, OrderCode.NEXT_VIDEO)
                Commands(self.eh).enqueue(header)
            elif button == mouse.Button.right:
                elapsed = (end_time - self.right_hold) * 1000
                print("Right mouse button released at",
                      end_time, "after", elapsed, "ms")
                header = MessageMan(self.eh).compose_message("", 0, OrderCode.LOOP_PLAY)
                Commands(self.eh).enqueue(header)
            elif button == mouse.Button.middle:
                elapsed = (end_time - self.middle_hold) * 1000
                print("Middle mouse button released at",
                      end_time, "after", elapsed, "ms")
                header = MessageMan(self.eh).compose_message("", 0, OrderCode.RESUME_SPEED)
                Commands(self.eh).enqueue(header)

    def on_scroll(self, x, y, dx, dy):
        """ Deals with mouse scroll wheel input.

        Args:
            x (int): x location of cursor when wheel scrolled.
            y (int): y location of cursor when wheel scrolled.
            dx (int): If -1, scroll left. If 1, scroll right.
            dy (int): If -1, scroll down. If 1, scroll up.

        Returns:
            bool: The return value. If True, continue listening. If False, stop listening.
        """
        if c_queue.IsLeader == False:
            raise
        
        if dy == 1:
            print("Scroll wheel up.")
            header = MessageMan(self.eh).compose_message("", 0, OrderCode.SPEED_UP)
            Commands(self.eh).enqueue(header)
        elif dy == -1:
            print("Scroll wheel down.")
            header = MessageMan(self.eh).compose_message("", 0, OrderCode.SLOW_DOWN)
            Commands(self.eh).enqueue(header)

    def on_move(self, x, y):
        """ Deals with moving the mouse.

        Args:
            x (int): x location of cursor when mouse moved
            y (int): y location of cursor when mouse moved

        Returns:
            None
        """
        if c_queue.IsLeader == False:
            raise
        
        # If we detect any mouse movement, move the mouse cursor to top left corner of the screen.
        self.mouse_controller.position = (0, 0)

    def input_listener(self):
        """ Listen for input from user.

        Args:
            None

        Returns:
            None
        """
        # This creates a daemon thread.
        listener = mouse.Listener(
            on_click=self.on_click, on_scroll=self.on_scroll)
        listener.start()
