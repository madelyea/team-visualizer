# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  Handles messages over ethernet, and splits them up in accordance with their codes, then
# stores the information in a usable form
#
# Sources: For timing, https://automatetheboringstuff.com/chapter15/
# ****************************************************

from ErrorHandler import ErrorHandler
from datetime import datetime
from datetime import timedelta
from constants import *
from OrderCode import OrderCode


class MessageMan:

    def __init__(self, error_handler=None):
        self.playlist = []
        self.start_time = float
        self.eh = error_handler
        self.start_time = None

    def parse_message(self, message_str) -> OrderCode:
        """ Responsible for parsing a message from another RasPi unit and 
        returning the command code

        Args: 
            message_str (string): The passed in message from another RasPi to 
            be parsed

        Returns:
            command (OrderCode): The command code of the message
        """
        message_list = message_str.split(',')
        try:
            command = message_list[0]
        except:
            self.eh.LogWarning("Error code could not be extracted from the message string")
            raise

        if command == 1:    # Command or response to begin shutdown procedures
            return command
        elif command == 2:  # Indicates that a new playlist is contained in the message
            # Note: We may actually want to use time.time() for better accuracy of runtime time.time() returns a float since the epoch
            # Extract start time
            try:
                self.start_time = float(message_list[1])
            except:
                self.eh.LogWarning("start time could not be extracted from the message string")
                raise
            # Extract song titles
            try:
                i = 2
                while i < len(message_list):
                    self.playlist.append(message_list[i])
                    i += 1
            except:
                self.eh.LogWarning("Error parsing the playlist")
                raise
            return command
        elif command == 3:  # Command to slowdown playback of videos
            return command
        elif command == 4:  # Command to speedup playback of videos
            return command
        elif command == 5:  # Command to remain on current video or visual
            return command
        elif command == 6:  # Command to return to normal playback mode
            return command
        elif command == 7:  # Command to resume normal playback speed of videos
            return command
        elif command == 8:  # For sending and receiving a response of connection
            return command
        elif command == 9:  # Command to suspend any visual projections
            return command
        else:
            # Unknown Error
            return 0
        return command

    def compose_message(self, input_message, start_time, order) -> dict:
        """ Responsible for composing a message for another RasPi unit and 
        returning the string to be sent

        Args: 
            input_message (string): The passed in message to give to another RasPi
            start_time (int): offset time for when the message will be executed
            order (OrderCode): the command code to be sent

        Returns:
            dict: The message ready to be sent in string form
            message =
                {
                    "code"       : (OrderCode),
                    "sent_time"  : (float),
                    "start_time" : (float),
                    "message"    : (str),
                    "file"       : (str),
                    "filesize"   : (int)
                }
        """
        message = {
            "code": (order.value),
            "sent_time": (datetime.now().timestamp()),
            "start_time": ((datetime.now() + timedelta(seconds=start_time)).timestamp()),
            "message": (input_message)
        }
        # Messageman needs a common sent time to send back for next_command.
        self.start_time = message['start_time']
        return message
