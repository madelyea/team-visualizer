# ****************************************************
# Copyright: 2020 Team Visualizer (Carlos Miguel Sayao, Connor Bettermann, Issac Greenfield, Madeleine Elyea, Tanner Sundwall, Ted Moore, Prerna Agarwal)
# License: MIT
# ****************************************************
# Purpose:  When an error happens call this class functions to log it.
#
# Sources:  https://stackoverflow.com/a/12158233
#
# Examples: ErrorHandler().log_error("test")
#           ErrorHandler().log_warning("test2")
#           ErrorHandler().log_info("tes3t")
#           ErrorHandler().log_critical("test4")
#           ErrorHandler(True).log_info("tes33333t")
# ****************************************************

from datetime import datetime
import logging
import pathlib

class ErrorHandler:

    # Class global variables
    def __init__(self, info=False):
        self.info = info
        self.log_file = (str(pathlib.Path(__file__).parent.absolute())
                         + "/Logging/Log_"
                         + datetime.now().strftime("%m-%d-%Y_%H-%M-%S-%f")
                         + ".log")
        self.remove_root_handlers()

    def remove_root_handlers(self):
        """ Removes all root handlers if any for the logging.

        Args:
            None

        Returns:
            None
        """
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)

    def log_error(self, message):
        """ Logs an Error message.

        Args:
            message (str): The message to log.

        Returns:
            None
        """

        logging.basicConfig(filename=self.log_file, filemode='w',
                            format='%(name)s - %(levelname)s - %(message)s')
        logging.error(str(message), exc_info=True)

    def log_warning(self, message):
        """ Logs a warning message.

        Args:
            message (str): The message to log.

        Returns:
            None
        """

        logging.basicConfig(filename=self.log_file, filemode='w',
                            format='%(name)s - %(levelname)s - %(message)s')
        logging.warning(str(message), exc_info=True)

    def log_info(self, message):
        """ Logs a info message.

        Args:
            message (str): The message to log.

        Returns:
            None
        """

        if self.info:
            logging.basicConfig(filename=self.log_file, filemode='w',
                                format='%(name)s - %(levelname)s - %(message)s',
                                level=logging.DEBUG)
            logging.info(str(message), exc_info=True)

    def log_critical(self, message):
        """ Logs a Critical message.

        Args:
            message (str): The message to log.

        Returns:
            None
        """

        logging.basicConfig(filename=self.log_file, filemode='w',
                            format='%(name)s - %(levelname)s - %(message)s')
        logging.critical(str(message), exc_info=True)
