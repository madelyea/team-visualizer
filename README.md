# READ ME

# Team Visualizer

A dual-pi visualizer program intended for use with projectors.

## Prerequisites

### Python

This project requires Python 3.6. You can install it at the following link:

[Installer](https://www.python.org/downloads/release/python-366/)

To check version:

```bash
$ python --version
```

### Project Hardware:

We used the Vilros RasPi4 bundle that included:
- Raspberry Pi 4, Model B microcomputer
- RasPi4 compatible hard-shell case
- Active-cooling fan unit
- Additionally, we added: 
- Adafruit PiRTC, PCF8523 battery-powered  clocks

### Project Software Licenses
- VLC Media Player Version
- The MIT License

## Raspberry Pi Software Setup:

Begin by mounting the microSD card: https://raspberrytips.com/install-raspbian-raspberry-pi/

Then update and boot-up the system: https://www.zdnet.com/article/raspberry-pi-4-model-b-and-raspbian-buster-how-to-set-up-your-board/ 

Running 2019-09-26 version of Raspbian buster.
> EDIT: Using Raspberry Pi 4

> NOTE - Do NOT upgrade OpenGL, this causes video playback errors.

Make sure to use:
```bash
sudo apt update
```
and not ‘apt-update’ because of when raspbian diverged from the stable debian release

## RTC Clock Module Setup:
Use the following link to setup RTC Clock: https://pimylifeup.com/raspberry-pi-rtc/


## Installation of our software:
Create a folder, “Visualizer” located at: `/home/pi/Visualizer`

Download our source code from: https://gitlab.com/madelyea/team-visualizer

Unzip it and place it into the Visualizer folder you just created

Upgrade pip3:
```bash
python3 -m pip install --upgrade pip
```
> ERROR: Invalid requirement: 'netifaces=0.10.9' (from line 1 of ../requirements.txt)
> Hint: = is not a valid operator. Did you mean == ?

## Running the Program

Install the libraries found in the `requirements.txt`
```bash
    pip3 install -r requirements.txt
```
Refer to the following link for starting VLC on boot: [Bootscript](https://gitlab.com/madelyea/team-visualizer/-/blob/class_outline/tests/bootscripts/README.md)

Here's the source for the above mentioned bootscipt: [Source](https://www.raspberrypi-spy.co.uk/2014/05/how-to-autostart-apps-in-rasbian-lxde-desktop/) 

## Supported video file formats:
Add all videos directly into the `/home/pi/Videos` folder that comes as a part of the Raspbian file structure.

Supported video file formats currently include: `.ogv`

As of 3/5/2020, VLC has issues with certain video file formats and video codecs when running on the Raspberry Pi 4. Through testing, `.ogv` video files provide the best quality with no unwanted artifacts.
Through this testing, we found that `.mp4`, `.h264`, `.mov`, `.avi`, `.mjpeg`, and `.cvid` file formats were all too unreliable. 

We have included a script `encode/convert.sh` that will convert all .avi video files within the `Videos` folder to compatible `.ogv` files.
Running this script will ensure that all videos will display smoothly at runtime.

To run the script, navigate to the `encode/` folder and run the following command:
```
sudo ./convert.sh
```

Any unsupported types will simply not be included in the playlist that our program compiles. Additionally, any media that is inclosed in their own folders in the Videos folder will be ignored.

## Visual Effects
Anaglyph visual effects are an option that can be added in VLCMan.py in its ```start_vlc()``` function.
This may cause some issues with display syncing between two Raspberry Pi's, but will look awesome.
Instead of:
```
Popen([dummy_vlc, fullscreen, mute, no_title, disable_scroll, loop,
                       disable_mouse, disable_network, cursor_timeout, single_instance, self.playlist])
```
You can instead add anaglyph to the argument list:
``` 
Popen([dummy_vlc, fullscreen, anaglyph, mute, no_title, disable_scroll, loop,
                       disable_mouse, disable_network, cursor_timeout, single_instance, self.playlist])
```

## Testing
For testing purposes, first set up a virtual environment.

### How to set up and use `virtual env`

Virtualenv creates a python environment in your current directory. To remove libraries from your computer, just delete the env/ dir.
```bash
    sudo apt install virtualenv

    virtualenv -p python3 env
```
This will install virtualenv using pip3 and create a an `env` folder that will contain the libraries brought it. `-p` indicates python version, `env` can be any name you want.
```bash
    source env/bin/activate
```
This will put your shell into the virtual environment where you will have the libraries brought it. You may have to use different `activate` depending on your shell. For instance sourcing with `fish` is `. env/bin/activate.fish`.
```bash
    pip3 install -r requirements.txt
```
This will install the libraries that are found in the `requirements.txt`. When you are done working, use `deactivate` to get out of the virtual environment.

# Team
- [Carlos Miguel Sayao](https://gitlab.com/carsayao)
- [Connor Bettermann](https://gitlab.com/connor26)
- [Issac Greenfield](https://gitlab.com/issacgreenfield)
- [Madeleine Elyea](https://gitlab.com/madelyea)
- [Tanner Sundwall](https://gitlab.com/Stanner)
- [Ted Moore](https://gitlab.com/DukeKamono)
- [Prerna Agarwal](https://gitlab.com/prerna214)
