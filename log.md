# Feb 22

## Problem

Leader doesn't want to wait for follower to process orders/files.

## Solution

Create a handshake method. The idea is that after every send() and send\_file(), the leader waits for the follower to send a confirmation message after follower's finished processing orders/files.

### New Implementations

- EtherTalk.py has an enum type for exclusive EtherTalk messages ('Talk.CONF').

- Connect, send, now have more expressive status returns (see docstrings).

### Known Issues

#### Issue 1

When testing leader to send many commands/files in a row (see tests/test\_leader.py and tests/test\_follower.py), leader tends to catch up to follower. So much so that follower must refuse connection, due to the limit of 5 passed into socket.listen(). A sleep of .5 placed at the top of connect() prevents leader from running over its listener.

How to mitigate this?

Leader needs to be in a truly blocked state. Need to find out how to ***get leader to chill*** in the recv() function until either response or timeout. For now, the sleep works.

***UPDATE*** (*RESOLVED*): Changed the sends to keep trying to connect while connection fails or until timeout. Also have sleep(.1) in connect. The following 3 arrays are return statuses of the two sending functions tested with test\_leader.py and test\_follower.py. The first array corresponds to 10 OrderCode sends; the second, 5 textfiles; and the third the same 10 codes along with a shutdown at the end. 

  [0, 1, 2, 2, 2, 2, 2, 2, 2, 2]
  [2, 2, 2, 2, 2]
  [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
  Finished in 2.6169 s
  
  Change: keep trying to connect while connection fails or until timeout
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  [0, 0, 0, 0, 0]
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  Finished in 12.7329 s
  
  ***Note***: when sleep is removed from connect
  [0, 3, 0, 3, 0, 3, 0, 3, 0, 3]
  [0, 3, 0, 3, 0]
  [3, 0, 3, 0, 0, 3, 0, 3, 0, 0, 0]
  Finished in 9.4425 s

Can we keep the socket open for the whole run?
